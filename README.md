Random build scripts
---

## pipeline.py

Usage: python pipeline.py -VDEVICE=mako -VVERSION=lineage-16.0

Expects `GITLAB_PROJECT`, `GITLAB_TRIGGER_TOKEN` and `GITLAB_ACCESS_TOKEN` as environment variables.