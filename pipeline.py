import argparse
import os
import time
import sys

import requests


def retry(func, *args, **kwargs):
    try:
        return func(*args, **kwargs)
    except Exception as e:
        print(f"Internal error, retrying: {e}")
        time.sleep(30)
        return retry(func, *args, **kwargs)

def start(project, trigger_token, variables):
    gitlab_url = f"https://gitlab.com/api/v4/projects/{project}/trigger/pipeline?token={trigger_token}&ref=master"
    data = {
        f"variables[{x}]": y for x,y in variables.items()
    } if variables else {}
    req = requests.post(gitlab_url, data)
    if req.status_code == 201:
        print("Started at ", req.json()['web_url'])
        return req.json()['id']
    else:
        print("ERROR", req.status_code, req.text)
        sys.exit(1)

def main():
    project = os.environ.get('GITLAB_PROJECT')
    trigger_token = os.environ.get("GITLAB_TRIGGER_TOKEN")
    access_token = os.environ.get("GITLAB_ACCESS_TOKEN")

    if not project or not trigger_token or not access_token:
        print('GITLAB_PROJECT, GITLAB_TRIGGER_TOKEN, and GITLAB_ACCESS_TOKEN required')
        sys.exit(1)

    parser = argparse.ArgumentParser()
    parser.add_argument('-V', action='append')

    variables = {x[0]:x[1] for x in [s.split("=") for s in parser.parse_args().V]} if parser.parse_args().V else {}
    print(variables)
    pipeline_id = start(project, trigger_token, variables)
    status = "pending"
    gitlab_url = f"https://gitlab.com/api/v4/projects/{project}/pipelines/{pipeline_id}"
    headers = {"Private-Token": access_token}
    while True:
        resp = retry(requests.get, gitlab_url, headers=headers)
        if resp.status_code == 200:
            status = resp.json().get("status")
            if status == "success":
                print("SUCCESS")
                sys.exit(0)
            elif status == "pending" or status == "running":
                print("Still waiting...")
            else:
                print("FAILURE")
                print(status)
                sys.exit(1)
        elif resp.status_code == 419:
            time.sleep(50)
        time.sleep(10)

if __name__ == "__main__":
    main()

